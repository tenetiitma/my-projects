package ee.sda.SDACodingLessons.Exercises;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class Exercise46 {
    public void solution(){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss z YYYY");
        Date date = new Date();

        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

        System.out.println("Current Date time: " + simpleDateFormat.format(date));
    }
}
