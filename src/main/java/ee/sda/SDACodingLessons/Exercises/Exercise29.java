package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise29 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a hexadecimal number:");
        int binaryNum = Integer.parseInt(scanner.nextLine(), 16);
        System.out.println("The binary value is: " + Integer.toBinaryString(binaryNum));
    }
}
