package ee.sda.SDACodingLessons.Exercises;

public class Exercise11 {
    public void solution(){

        final double radius = 7.5;

        double area = Math.PI * radius * radius;
        double perimeter = 2 * Math.PI * radius;

        System.out.println("Perimeter is " + perimeter + " and area is " + area);

    }
}
