package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise53 {
    public void solution(){
//        Write a Java program that accepts three integers from the user and return
//        true if the second number is greater than first number and third number is
//        greater than second number.
        /**
         * If "abc" is true second number does not need to
         *         be greater than first number.
         *
         * I DON'T UNDERSTAND THIS PART? WHAT DO THEY MEAN BY "if abc is true"?
         */


        Scanner scanner = new Scanner(System.in);

        System.out.println("Input the first number: ");
        int a = scanner.nextInt();
        System.out.println("Input the second number: ");
        int b = scanner.nextInt();
        System.out.println("Input the third number: ");
        int c = scanner.nextInt();

        if (b > a && c > b)
            System.out.println("The result is: true");

    }
}
