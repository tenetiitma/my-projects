package ee.sda.SDACodingLessons.Exercises;

import java.nio.charset.Charset;

public class Exercise40 {
    public void solution(){
        System.out.println("List of available character sets: ");

        for (String str : Charset.availableCharsets().keySet()) {
            System.out.println(str);
        }
    }
}
