package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise24 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a binary number:");
        int bin = Integer.parseInt(scanner.nextLine(), 2);
        System.out.println("The octal value is: " + Integer.toOctalString(bin));
    }
}
