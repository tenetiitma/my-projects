package ee.sda.SDACodingLessons.Exercises;

import java.io.File;

public class Exercise45 {
    public void solution(){

//        /home/students/abc.txt : 0 bytes
//                /home/students/test.txt : 0 bytes

        System.out.println("/home/students/abc.txt : " + new File("abc.txt").length() + " bytes");
        System.out.println("/home/students/test.txt : " + new File("test.txt").length() + " bytes");
    }
}
