package ee.sda.SDACodingLessons.Exercises;


public class Exercise15 {
    public void solution(){
        int a, b, temp;

        a = 24;
        b = 16;
        System.out.println("Numbers before swapping: " + a + " , " + b);

        temp = a;
        a = b;
        b = temp;
        System.out.println("Numbers after swapping: " + a + " , " + b);
    }
}
