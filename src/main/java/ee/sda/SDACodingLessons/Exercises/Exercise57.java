package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise57 {
    public void solution() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please input an integer:");
        int input = scanner.nextInt();

        System.out.println(result(input));
    }

    public static int result(int num) {
        int ctr = 0;
        for (int i = 1; i <= (int) Math.sqrt(num); i++) {
            if (num % i == 0 && i * i != num) {
                ctr += 2;
            } else if (i * i == num) {
                ctr++;
            }
        }
        return ctr;
    }
}