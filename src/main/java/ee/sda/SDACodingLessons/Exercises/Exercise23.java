package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise23 {
    public void solution() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a binary number:");
        int hexadecimalNum = Integer.parseInt(scanner.nextLine(), 2);
        System.out.println("The hexadecimal value is: " + Integer.toHexString(hexadecimalNum));

    }
}
