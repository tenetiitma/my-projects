package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise62 {
    public void solution(){
//        Write a Java program that accepts three integer values and return true if
//        one of them is 20 or more and less than the substractions of others.

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please input first integer: ");
        int a = scanner.nextInt();
        System.out.println("Please input second integer: ");
        int b = scanner.nextInt();
        System.out.println("Please input third integer: ");
        int c = scanner.nextInt();

        System.out.println((Math.abs(a-b) >= 20 || Math.abs(b-c) >= 20 || Math.abs(c-a) >= 20));
    }
}
