package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise22 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a binary number:");
        int decimalNum = Integer.parseInt(scanner.nextLine(), 2);
        System.out.println("The decimal value is: " + decimalNum);

    }
}