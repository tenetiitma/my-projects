package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise18 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter first binary number:");
        int firstBinary = scanner.nextInt();

        System.out.println("Please enter second binary number:");
        int secondBinary = scanner.nextInt();

        System.out.println("Multiplying these two numbers add up to: " + (firstBinary * secondBinary));


    }
}
