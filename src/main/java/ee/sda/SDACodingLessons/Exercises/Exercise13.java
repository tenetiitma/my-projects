package ee.sda.SDACodingLessons.Exercises;

public class Exercise13 {
    public void solution(){

        double width = 5.5d;
        double height = 8.5d;

        System.out.println("The perimeter is " + (2 * (width + height)));
        System.out.println("The area is " + (width * height));

    }

}
