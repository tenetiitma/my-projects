package ee.sda.SDACodingLessons.Exercises;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Exercise47 {
    public void solution(){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY/MM/DD HH:MM:SS.ms");
        Date date = new Date();

        System.out.println("Now: " + simpleDateFormat.format(date));
    }
}
