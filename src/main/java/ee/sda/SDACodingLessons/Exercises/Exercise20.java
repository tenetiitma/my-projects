package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise20 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a decimal number: ");
        System.out.println("Hexadecimal value is: " + Integer.toHexString(scanner.nextInt()));

    }
}

