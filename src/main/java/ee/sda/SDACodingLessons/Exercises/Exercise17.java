package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise17 {

    public void solution(){
        long binary1, binary2;
        int i = 0;

        int[] sum = new int[20];
        Scanner scanner = new Scanner(System.in);

        System.out.println("Insert first binary:");
        binary1 = scanner.nextLong();
        System.out.println("Insert second binary:");
        binary2 = scanner.nextLong();

        i = addTwoBinaryNumbers(binary1, binary2, i, sum);

        --i;

        System.out.println("Sum of two binary numbers: ");
        while (i >= 0) {
            System.out.println(sum[i--]);
        }
    }

    // Should be less than 150chars in one line.
    // Make number of params up to 4, but 2 is perfect!
    private int addTwoBinaryNumbers(long binary1,
                                    long binary2,
                                    int i,
                                    int[] sum) {
        int reminder = 0;

        while  (binary1 != 0 || binary2 != 0){

            sum[i++] = (int)((binary1 % 10 + binary2 % 10 + reminder) % 2);
            reminder = (int)((binary1 % 10 + binary2 % 10 + reminder) / 2);

            binary1 = binary1 / 10;
            binary2 = binary2 / 10;
        }

        if (reminder != 0){
            sum[i++] = reminder;
        }
        return i;
    }
}

//  10
//  +
//  11
//  __
//  101
