package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise52 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input the first number: ");
        int first = scanner.nextInt();
        System.out.println("Input the second number: ");
        int second = scanner.nextInt();
        System.out.println("Input the third number: ");
        int third = scanner.nextInt();

        if (first + second == third)
            System.out.println("The result is: true");
    }
}
