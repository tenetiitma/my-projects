package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise54 {
    public void solution() {
//        Write a Java program that accepts three integers from the user and return
//        true if two or more of them (integers ) have the same rightmost digit. The
//        integers are non-negative.
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input first number: ");
        int num1 = scanner.nextInt();
        System.out.println("Input second number: ");
        int num2 = scanner.nextInt();
        System.out.println("Input third number: ");
        int num3 = scanner.nextInt();
        System.out.println("The result is: " + last_digit(num1, num2, num3, true));
    }
        public static boolean last_digit(int p, int q, int r, boolean num1num2num3){
            return (p % 10 == q % 10 || p % 10 == r % 10 || p % 10 == r % 10);
        }
        }

