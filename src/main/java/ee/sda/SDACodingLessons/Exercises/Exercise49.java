package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise49 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a number: ");
        int num = scanner.nextInt();

        if (num % 2 == 0){
            System.out.println("1");
        }
        else {
            System.out.println("0");
        }
    }
}
