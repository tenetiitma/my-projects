package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise60 {
    public void solution(){
//        Write a Java program to find the penultimate (next to last) word of a
//        sentence.

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a sentence: ");
        String sentence = scanner.nextLine();

        String[] words = sentence.split("[ ]+");
        System.out.println("Penultimate word is: " + words[words.length - 2]);

    }
}
