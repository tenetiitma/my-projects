package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise26 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter an octal number:");
        int bin = Integer.parseInt(scanner.nextLine(), 8);
        System.out.println("The binary value is: " + Integer.toBinaryString(bin));
    }
}
