package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise27 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter an octal number: ");
        int hex = Integer.parseInt(scanner.nextLine(), 8);
        System.out.println("The hexadecimal value is: " + Integer.toHexString(hex));
    }
}
