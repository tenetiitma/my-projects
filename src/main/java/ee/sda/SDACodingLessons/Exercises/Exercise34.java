package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise34 {
    public void solution() {
        // Area of a hexagon = (6 * s^2)/(4*tan(π/6))
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please input hexagon side length: ");
        double s = scanner.nextDouble();

        double area = (6 * s*s)/(4* Math.tan(Math.PI/ 6));

        System.out.println("The area of a hexagon is: " + area);

    }
}
