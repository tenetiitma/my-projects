package ee.sda.SDACodingLessons.Exercises;

public class Exercise48 {
    public void solution(){
        // Write a Java program to print the odd numbers from 1 to 99. Prints one
        //number per line.

        for (int n = 1; n <=99; n++) {
            if (n % 2 != 0){
                System.out.println(n + " ");
            }
        }
    }
}
