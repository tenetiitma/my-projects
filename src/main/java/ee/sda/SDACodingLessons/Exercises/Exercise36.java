package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise36 {
    public void solution() {
//        Distance between the two points [ (x1,y1) & (x2,y2)]
//        d = radius * arccos(sin(x1) * sin(x2) + cos(x1) * cos(x2) * cos(y1 - y2))
//        Radius of the earth r = 6371.01 Kilometers
        Scanner scanner = new Scanner(System.in);

        System.out.print("Please input the latitude of coordinate 1: ");
        double x1 = scanner.nextDouble();
        System.out.print("Please Input the longitude of coordinate 1: ");
        double x2 = scanner.nextDouble();
        System.out.print("Please input the latitude of coordinate 2: ");
        double y1 = scanner.nextDouble();
        System.out.print("Please Input the longitude of coordinate 1: ");
        double y2 = scanner.nextDouble();

        double r = 6371.01;

        System.out.println("The distance between these two points is: " +
                distanceBetweenLatLong(x1, x2, y1, y2) + " km");
    }

    public static double distanceBetweenLatLong(double x1, double x2, double y1, double y2) {
        x1 = Math.toRadians(x1);
        x2 = Math.toRadians(x2);
        y1 = Math.toRadians(y1);
        y2 = Math.toRadians(y2);

        double earthRadius = 6371.01;
        return earthRadius * Math.acos(Math.sin(x1) * Math.sin(y1) + Math.cos(x1) * Math.cos(y1) * Math.cos(x2 - y2));
    }
}
