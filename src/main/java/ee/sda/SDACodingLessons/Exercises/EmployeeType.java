package ee.sda.SDACodingLessons.Exercises;

public abstract class EmployeeType {

    abstract int payMonthlyAmount();
}
