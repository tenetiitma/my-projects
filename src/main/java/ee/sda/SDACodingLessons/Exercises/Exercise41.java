package ee.sda.SDACodingLessons.Exercises;


import java.util.Scanner;

public class Exercise41 {
    public void solution(){

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a character: ");
        int character = scanner.next().charAt(0);

        int castAscii = (int) character;

        System.out.println("The ascii value of the given character is: " + castAscii);




//        char character = 'Z';
//        int ascii = character;
//
//        System.out.println("The ascii value of " + character + " is " + ascii);
    }
}
