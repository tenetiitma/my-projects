package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise55 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please input number of seconds: ");
        int seconds = scanner.nextInt();

        int h = seconds % 60;
        int m = seconds / 60;
        int s = m % 60;

        m = m / 60;

        System.out.println("The time is: " + m + ":" + s + ":" + h);
    }
}
