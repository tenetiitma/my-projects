package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise42 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter your password: ");
        String pw = scanner.nextLine();
        System.out.println("Your password was: " + pw);
    }
}
