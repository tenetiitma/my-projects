package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Exercise33 {
    public void solution() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter an integer: ");
        int integer = scanner.nextInt();

        System.out.println("The sum of the digits is: " + sumDigits(integer));
    }
        public static int sumDigits(long n) {
            int sum = 0;
            while (n != 0) {
                sum += n % 10;
                n /= 10;
            }
            return sum;
        }
}
