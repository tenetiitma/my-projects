package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise28 {
    public void solution() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a hexadecimal number:");
        int decimalNum = Integer.parseInt(scanner.nextLine(), 16);
        System.out.println("The decimal value is: " + decimalNum);
    }
}
