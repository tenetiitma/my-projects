package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise32 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please input first integer: ");
        int firstInt = scanner.nextInt();
        System.out.println("Please input second integer: ");
        int secondInt = scanner.nextInt();

        if (firstInt != secondInt) {
            System.out.println(firstInt + " != " + secondInt);
        }
        if (firstInt < secondInt) {
            System.out.println(firstInt + " < " + secondInt);
        }
        if (firstInt <= secondInt) {
            System.out.println(firstInt + " <= " + secondInt);
        }
        if (firstInt == secondInt) {
            System.out.println(firstInt + " = " + secondInt);
        }
        if (firstInt > secondInt) {
            System.out.println(firstInt + " > " + secondInt);
        }
        if (firstInt >= secondInt) {
            System.out.println(secondInt + " => " + secondInt);
        }
    }
}
