package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise21 {
    public void solution(){

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a decimal number:");
        System.out.println("The octal value is: " + Integer.toOctalString(scanner.nextInt()));

    }
}
