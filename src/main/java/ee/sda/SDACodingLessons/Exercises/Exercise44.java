package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise44 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please input a number: ");
        int n = scanner.nextInt();

        System.out.println(n + " + " + n + n + " + " + n + n + n);
    }
}
