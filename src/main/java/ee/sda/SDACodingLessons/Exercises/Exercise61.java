package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise61 {
    public void solution(){
//      Write a Java program to reverse a word

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a word: ");
        String word = scanner.nextLine();

        word = word.trim();

        String result = "";

        char[] ch = word.toCharArray();

        for (int i = ch.length - 1; i >= 0; i--) {
            result += ch[i];
        }
        System.out.println("Reversed word is: " + result.trim());
    }
}
