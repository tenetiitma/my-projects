package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise51 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input a number (string): ");
        String input = scanner.nextLine();
        int integer = Integer.parseInt(input);

        System.out.println("The integer value is: " + integer);
    }
}
