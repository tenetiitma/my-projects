package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise35 {
    public void solution() {
        // Area of a polygon = (n*s^2)/(4*tan(π/n))
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the amount of sides in polygon:");
        double n = scanner.nextDouble();
        System.out.println("Please enter the side length of the polygon:");
        double s = scanner.nextDouble();

        double area = (n* s * s) / (4 * Math.tan(Math.PI / n));

        System.out.println("The area of the polygon is: " + area);
    }
}
