package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise12 {
    public void solution(){

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter first number:");
        float a = scanner.nextInt();
        System.out.println("Please enter second number:");
        float b = scanner.nextInt();
        System.out.println("Please enter third number:");
        float c = scanner.nextInt();

        System.out.println("The average of the entered numbers is: " + ((a + b + c) / 3));

    }
}
