package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise7 {

    public void solution() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a number:");

        int num = scanner.nextInt();

        for (int i = 0; i < 10; i++) {
            System.out.println(num + " x " + (i+1) + " = " + (num * (i+1)) );
        }
    }
}
