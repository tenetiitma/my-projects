package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise25 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter an octal number:");
        String octalString = scanner.nextLine();
        int decimal = Integer.parseInt(octalString, 8);
        System.out.println("The decimal value is: " + decimal);
    }
}
