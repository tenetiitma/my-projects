package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise30 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a hexadecimal number: ");
        int octalNum = Integer.parseInt(scanner.nextLine(), 16);
        System.out.println("The octal value is: " + Integer.toOctalString(octalNum));

    }
}
