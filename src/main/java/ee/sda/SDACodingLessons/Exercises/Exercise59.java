package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise59 {
    public void solution(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please input a sentence: ");
        String sentence = scanner.nextLine();

        String lower_case_sentence = "";

        Scanner sentenceScan = new Scanner(sentence);

        while(sentenceScan.hasNext()) {
            String word = sentenceScan.next();
            lower_case_sentence += Character.toLowerCase(word.charAt(0)) + word.substring(1) + " ";
        }
        System.out.println(lower_case_sentence.trim());
    }
}
