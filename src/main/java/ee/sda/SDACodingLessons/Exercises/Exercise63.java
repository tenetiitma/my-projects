package ee.sda.SDACodingLessons.Exercises;

import java.util.Scanner;

public class Exercise63 {
    public void solution(){
//        Write a Java program that accepts two integer values from the user and
//        return the larger values. However if the two values are the same, return 0 and
//        return the smaller value if the two values have the same remainder when
//        divided by 6.
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter first integer: ");
        int a = scanner.nextInt();
        System.out.println("Please enter second integer: ");
        int b = scanner.nextInt();
        System.out.println("Result: " + result(a,b));
    }
    public static int result(int x, int y) {
        if (x == y)
            return 0;
        if (x % 6 == y % 6)
            return (x < y) ? x : y;
            return (x > y) ? x : y;
    }
}
