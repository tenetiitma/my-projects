package ee.sda.SDACodingLessons.OOP;

public class Inheritance {
    //Inheritance - take something after mother, father, grandmother etc.

    public static void main(String[] args) {

        JavaProgrammer javaProgrammer = new JavaProgrammer();
        javaProgrammer.code();
        boolean isJava = javaProgrammer.code("C++");

        System.out.println(isJava);


        String programming = "test";
        System.out.println(programming.indexOf("te"));



    }
//        Car car = new Car();
//        car.setColor("Blue");
//        car.setName("X6");
//
//
//        BMWCar bmwCar = new BMWCar("X", "Black", "7");
//
//        bmwCar.getColor();
//        bmwCar.getName();
//
//        //  Abstraction
//        //  We don't have to show these things to the user.
//
//        // 1. Abstract class.
//        // 2. Creating Interface.
//
//        Shape shape = new Shape() {
//            public void draw() {
//
//            }
//
//            public void clean() {
//
//            }
//        };
//
//        Circle circle = new Circle();
//        circle.draw();
//
//        }
    }

