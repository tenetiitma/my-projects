package ee.sda.SDACodingLessons.OOP;
// Base class or parent class
abstract class Motorcycle {


    // Object Oriented Programming
    // 1. Encapsulation - make all properties of an object private and create setters and getters.
    // 2. Inheritance - when an object gets all it's properties of parent object
    // 3. Polymorphism - when you have overloading and overwriting.
    // 4. Abstraction - when you create abstract method/property. Use when you want to hide something from user.



    // Properties (or States, like speed):
    private String model;
    private String color;
    private double speed; // it can be changed, state.
    // The rest of properties

    // Behaviors - methods.
    public int drive(int km, int speed){
        this.speed = speed;
        return km/speed;
    }

    // Overloading - creating additional things that we are adding to the class.
    // Creating something with different parameters/arguments.
    public int drive(int speed){
        this.speed = speed;
        return 2;
    }

    protected void addGas(int gallons) {

    }

    abstract double getTopSpeed();

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }
}
