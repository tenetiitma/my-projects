package ee.sda.SDACodingLessons.OOP;

public class Encapsulation {

    public static void main(String[] args) {
        //Creating objects
        Cat thor = new Cat("Thor", 6, "Blue");
        Cat rambo = new Cat("Rambo", 3, "Brown");

//        thor.name = "THOR";
//        thor.age = 3;
//        thor.color = "Red";
        thor.setPrice("200 EUR");
        thor.play();

        //Encapsulation - process of wrapping a code or data together. Use setter or getter methods.
        // 1. Make all properties of an object as private.
        // 2. Create two kind of methods called setter and getter.
//
//        thor.setName("Bolt");
//        thor.setAge(5);

        System.out.println("Thors age is " + thor.getAge());
    }
}
