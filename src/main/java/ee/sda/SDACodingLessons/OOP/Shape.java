package ee.sda.SDACodingLessons.OOP;
// ABSTRACT EXAMPLE
abstract class Shape {

    //You don't need to use public or private.
    // You can use arguments if you want in ().

    // Draws something.
    public abstract void draw();
    //if it's abstract, it has no body.
    public abstract void clean();
}
