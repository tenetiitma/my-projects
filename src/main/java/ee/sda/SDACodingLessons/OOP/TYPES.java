package ee.sda.SDACodingLessons.OOP;

import java.math.BigDecimal;

public class TYPES {
    public static void main(String[] args) {


        //Primitive type:
        //int , long, float, double, char, boolean

        //Wrapper types, immutable classes
        //String, Integer, Long, DigDecimal...

        Integer integer = 178;
        integer.doubleValue();

        int pInteger = 178;

        // 13.57 - money

        double f = 15.8;

        BigDecimal bigDecimal = new BigDecimal(50);
        bigDecimal.multiply(new BigDecimal(30.8));

        String s = new String();
        String a = "";


    }
}
