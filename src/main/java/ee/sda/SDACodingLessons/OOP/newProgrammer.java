package ee.sda.SDACodingLessons.OOP;

public class newProgrammer {
    public static void main(String[] args) {
        //JavaProgrammer javaProg = new JavaProgrammer();
        //JavaProgrammer javaProg2 = new JavaProgrammer();
        //
        //javaProg.simpleMethod();
        //javaProg2.simpleMethod();
        //
        // Doesn't need "new" class made.
        JavaProgrammer.staticMethod();
    }
}
