package ee.sda.SDACodingLessons.OOP;

public class JavaProgrammer extends Programmer {
    // Polymorphism - one object can take many forms.
    // 1. Overriding
    // 2. Overloading

    // relationships between two or more classes.
    // A - concactinfo , B - student ; example is HAS-A (Student has contactinfo)

    // Aggregation - two classes have a relationship, but they can exist independently.
    //               information can exist without each other.
    // Composition - two classes have a relationship, but one can't exist without the other.


    @Override
    public void code() {
        System.out.println("Coding in C++");
    }

    public static void staticMethod(){
    }

    public void simpleMethod(){
    }

}
