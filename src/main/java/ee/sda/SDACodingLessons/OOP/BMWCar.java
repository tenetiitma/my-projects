package ee.sda.SDACodingLessons.OOP;

public class BMWCar extends Car { // Extend is overriding parent class objects.

//    private String name;          These are already inherited from Car.
//    private String color;
    private String version;

    //Constructor
    public BMWCar(String name,
                  String color,
                  String version) {
        super(name, color); // If i need to override something in the child class from the parent class.
                            // I want to assign name and color for the parent class. it is MANDATORY!
        //Super calls parents class.
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
