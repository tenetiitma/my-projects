package ee.sda.SDACodingLessons.OOP;

public class Circle extends Shape {

    public void draw() {
        System.out.println("Circle!");
    }

    @Override
    // Annotation - we are reusing clean method inside the circle.
    public void clean() {

    }
}
