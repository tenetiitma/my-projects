package ee.sda.SDACodingLessons.OOP;

public class Car {

    private String name;
    private String color;
    private String anotherField;

    public Car() {
    }

    public Car(String name,
               String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.anotherField = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
