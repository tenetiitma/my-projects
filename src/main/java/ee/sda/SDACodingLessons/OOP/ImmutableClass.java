package ee.sda.SDACodingLessons.OOP;

public class ImmutableClass {

    private static double  x=4; // (3.4, 5.8)
    private static double  y=5;

    private ImmutableClass() {
    }

//    private ImmutableClass(double x, double y) {
//        this.x = x;
//        this.y = y;
//    }

    public static double getX() {
        return x;
    }

    public static double getY() {
        return y;
    }
}
