package ee.sda.SDACodingLessons.OOP;

public class Programmer {

    //OVERLOADING
    // 1. Parameters should be different!
    // 2. They can have different return types.
    public void code(){
        System.out.println("Coding in Java");
    }

    public boolean code(String language){
        System.out.println("Coding in " + language);
        // Ternary formula: condition ? statement1 : statement2;
        return language.equals("Java");
    }
}
