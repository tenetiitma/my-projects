package ee.sda.SDACodingLessons.OOP;

public class Company {
    private String name;
    private Employee[] employees = new Employee[MAX_NUM_EMPLOYEES];
    public final static int MAX_NUM_EMPLOYEES = 10;

    // lists are hiding
      public Company(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee[] getEmployees() {
        return employees;
    }

    public void setEmployees(Employee[] employees) {
        this.employees = employees;
    }
}
