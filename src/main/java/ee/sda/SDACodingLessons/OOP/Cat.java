package ee.sda.SDACodingLessons.OOP;

public class Cat {

    //An object is an entity that has states and behaviours.
    //State tells us how the object look and what properties it has.

    //States
    //MANDATORY FIELDS
    private String name;
    private int age;
    private String color;
    //OPTIONAL FIELD
    private String price;

    public Cat(String name,
               int age,
               String color) {
        this.name = name;
        this.age = age;
        this.color = color;
    }

    //Behaviours
    void sleep() {
        System.out.println("Sleeping");
    }

    void play() {
        System.out.println("Playing");
    }

    // 2 methods of setting and getting.

    //MANUAL METHOD - writing it all yourself.

    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }


    //AUTOMATED METHOD
        // alt + insert - setters and getters

    //NB! if you remove "SetName, etc" it will make it "read only". If you remove getters, you will get "write only".

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        //Any change
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
