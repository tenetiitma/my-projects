package ee.sda.SDACodingLessons.OOP;

public class BMWMotorcycle extends Motorcycle {



    public void testMethod(){
        String color = getColor();

        addGas(13);
    }

    @Override
    public int drive(int km, int speed) {
        return km * speed;
    }

    @Override
    double getTopSpeed() {
        return 250.0;
    }
}
