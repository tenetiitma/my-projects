package ee.sda.SDACodingLessons.JavaAdvancedExercises;

import ee.sda.SDACodingLessons.JavaAdvancedExercises.Task3.Square;

public class Main {
    public static void main(String[] args) {

       // Testing the square implementation.
        Square square = new Square("white", false, 8);

        System.out.println("Area of the given square is: " + square.getArea());
    }
}
