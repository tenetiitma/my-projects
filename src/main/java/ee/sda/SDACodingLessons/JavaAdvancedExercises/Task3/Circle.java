package ee.sda.SDACodingLessons.JavaAdvancedExercises.Task3;

public class Circle extends Shape {

    private float radius;

    public Circle() {
        super ("Unknown", false);
        this.radius = 1;
    }

    public Circle(String color, boolean isFilled, float radius) {
        super(color, isFilled);
        this.radius = radius;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float getArea() {
        return (float) (Math.PI * radius * radius);
    }

    public float getPerimeter() {
        return (float) (Math.PI * 2 * radius);
    }

    @Override
    public String toString() {
        return String.format( "Circle with radius = %f which is a subclass off %s", radius, super.toString());
    }
}
