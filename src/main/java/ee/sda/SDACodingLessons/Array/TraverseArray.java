package ee.sda.SDACodingLessons.Array;

public class TraverseArray {
    //LOOPS
    public void printArrayElements(int[] arr){
        for (int i = 0; i < arr.length; i++) {
            System.out.println(i + " position is " + arr[i]);
        }
    }
}
