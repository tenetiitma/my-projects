package ee.sda.SDACodingLessons.oopandarray;

import ee.sda.SDACodingLessons.OOP.Company;
import ee.sda.SDACodingLessons.OOP.Employee;
import ee.sda.SDACodingLessons.OOP.ImmutableClass;

import java.awt.event.ItemListener;

import static ee.sda.SDACodingLessons.OOP.Company.MAX_NUM_EMPLOYEES;

public class Main {

    public static void main(String[] args) {

        Company newCompany = new Company("BMW");

        Employee employee1 = new Employee("John", "Jonson", 32);
        Employee employee2 = new Employee("Tracy", "Macks", 22);
        Employee employee3 = new Employee("Max", "Lemon", 36);

        Employee[] employeeList = new Employee[MAX_NUM_EMPLOYEES];
        employeeList[0] = employee1;
        employeeList[1] = employee2;
        employeeList[2] = employee3;


        newCompany.setEmployees(employeeList);

        // Show all employees first name
        EmployeeTraverse employeeTraverse = new EmployeeTraverse();
        employeeTraverse.showAlEmployeesFirstName(newCompany.getEmployees());

        //
        EmployeeSearch employeeSearch = new EmployeeSearch();
        Employee foundEmployee = employeeSearch.findEmployeeByFirstName(employeeList, "John");

        // Null point exception, we should handle it.

        if(foundEmployee != null){
//            System.out.println();
            System.out.println("\n\nFound employees age is: ");
            System.out.println(foundEmployee.getAge());
        }

// Insert new employee
        EmployeeInsert employeeInsert = new EmployeeInsert();
        Employee employee4 = new Employee("Mark", "Twain", 50);

        employeeInsert.addNewEmployee(employeeList, employee4);

        // Show all employees again after insertion.

        System.out.println("\nUpdated list after insertion: \n");
        employeeTraverse.showAlEmployeesFirstName(employeeList);

        EmployeeDelete employeeDelete = new EmployeeDelete();
        System.out.println("\n\nNew updated list after deleted given employee: ");
        employeeDelete.deleteEmployee(employeeList, employee4);

        employeeTraverse.showAlEmployeesFirstName(employeeList);

        // Now we can sort employees by their ages.

        EmployeeSort employeeSort = new EmployeeSort();
        Employee[] sortedEmployee = employeeSort.sortEmployeesByAge(employeeList);

        System.out.println("\nUpdated list sorted by age: ");

        employeeTraverse.showAlEmployeesFirstName(sortedEmployee);

        Employee[] employeeListForBDepartment = new Employee[MAX_NUM_EMPLOYEES];

        employeeListForBDepartment[0] = new Employee("Sam", "Something", 65);
        employeeListForBDepartment[1] = new Employee("Kelsea", "Kelp", 61);
        employeeListForBDepartment[2] = new Employee("Bob", "Bobcat", 28);

//        Merge operation
//        employeeList + employeeListForBDepartment

        EmployeeMerge employeeMerge = new EmployeeMerge();
        Employee[] mergedEmployees = employeeMerge.mergeTwoEmployeesList(employeeList, employeeListForBDepartment);

        System.out.println("\n\nNew updated list after merging two employees list: ");

        employeeTraverse.showAlEmployeesFirstName(mergedEmployees);

        employeeSort.sortEmployeesByAge(mergedEmployees);

        System.out.println("\n\nNew updated list after merging two employees list and sorted by age: ");

        employeeTraverse.showAlEmployeesFirstName(mergedEmployees);

    }
}
