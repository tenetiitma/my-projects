package ee.sda.SDACodingLessons.oopandarray;

import ee.sda.SDACodingLessons.OOP.Employee;

public class EmployeeDelete {

    public boolean deleteEmployee(Employee[] employees,
                                  Employee employeeRemoval){
        for (int i = 0; i < employees.length; i++) {
            if(employees[i] != null && employees[i].getFirstName().equals(employeeRemoval.getFirstName())){
                employees[i] = null;
                return true;
            }
        }
        return false;
    }
}
