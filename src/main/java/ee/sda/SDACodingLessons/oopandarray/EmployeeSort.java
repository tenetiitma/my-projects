package ee.sda.SDACodingLessons.oopandarray;

import ee.sda.SDACodingLessons.OOP.Employee;

public class EmployeeSort {

    // There are some sorting algorithms:
    // 1. Bubble sort
    // 2. Insertion sort
    // 3. Quick sort
    // ....

    // Bubble
    // 34, 24, 38, 15
    // 24, 34, 38, 15
    // 24, 34, 15, 38
    // 24, 15, 34, 38
    // 15, 24, 34, 38
    public Employee[] sortEmployeesByAge(Employee[] employees){

        // Outer loop
        for (int i = 0; i < employees.length; i++) {
            // Inner loop
            for (int j = 0; j < employees.length - 1 - i; j++) {
                // a != null && b != null => a == 0 || b == null
                int nextElementIndex = j + 1;

                if(employees[j] == null || employees[nextElementIndex] == null) {
                   continue;
                }

                if(employees[j].getAge() > employees[nextElementIndex].getAge()){
                        swapEmployees(employees, j);
                }
            }
        }
        return employees;
    }

    private void swapEmployees(Employee[] employees, int j) {
        Employee temp = employees[j];
        employees[j] = employees[j + 1];
        employees[j + 1] = temp;
    }
}
