package ee.sda.SDACodingLessons.oopandarray;

import ee.sda.SDACodingLessons.OOP.Employee;

public class EmployeeSearch {

    public Employee findEmployeeByFirstName(Employee[] employees,
                                          String firstName){
        for (int i = 0; i < employees.length; i++) {
            if(employees[i].getFirstName().equals(firstName)){
                return employees[i];
            }
        }
        return null;
    }

    // Add more methods to search.
}
