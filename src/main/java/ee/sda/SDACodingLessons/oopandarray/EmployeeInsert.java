package ee.sda.SDACodingLessons.oopandarray;

import ee.sda.SDACodingLessons.OOP.Employee;

import static ee.sda.SDACodingLessons.OOP.Company.MAX_NUM_EMPLOYEES;

public class EmployeeInsert {

    public Employee[] addNewEmployee(Employee[] employees,
                                     Employee newEmployee) {

        // John, tracy, max, null, null, null, null, null, null
            for (int i = 0; i < employees.length; i++) {
                if (employees[i] == null) {
                    employees[i] = newEmployee;
                    break;
                }
            }
        return employees;
    }
}
