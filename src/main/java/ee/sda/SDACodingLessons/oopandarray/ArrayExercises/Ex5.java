package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

public class Ex5 {
//  Write a Java program to test if an array contains a specific value.
    public void solution(){
        int[] numbers = {1, 53, 683, 96, 29};
        int contains = 683;
        boolean found = false;

        for (int n : numbers) {
            if (n == contains) {
                found = true;
                break;
            }
        }
        if (found)
            System.out.println("Given array contains: " + contains);
        else
            System.out.println("Given array does not contain: " + contains);
    }
}
