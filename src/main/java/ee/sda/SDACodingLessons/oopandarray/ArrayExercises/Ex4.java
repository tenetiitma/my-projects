package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

public class Ex4 {
    public void solution(){
//         Write a Java program to calculate the average value of array elements.

        double[] array = {12, 5, 26, 27, 89};

        double sum = 0;

        for (int i = 0; i < array.length; i++) {
            sum = sum + array[i];
        }

        double average = sum / array.length;

        System.out.println("The average value of the array elements is: " + average);
    }
}
