package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;
import java.util.HashSet;

public class Ex15 {
    public void solution() {
//        Write a Java program to find the common elements between two arrays of
//        integers.
        
        int[] array1 = {1, 5, 6, 34, 643, 24, 245, 7};
        int[] array2 = {2, 6, 2, 7, 643, 26, 254, 65};

        System.out.println("First array: " + Arrays.toString(array1));
        System.out.println("Second array: " + Arrays.toString(array2));

        HashSet<Integer> set = new HashSet<Integer>();

        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == (array2[j])) {
                    System.out.println("Common element is: " + array1[i]);
                }
            }
        }
    }
}
