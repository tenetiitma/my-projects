package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;

public class Ex10 {
    public void solution() {
//        Write a Java program to find the maximum and minimum value of an
//        array.

       int givenArray[] = {25, 96, 9382, 6839, 8, 9320, 948, 95} ;
       int min = givenArray[0];
       int max = givenArray[0];
       int length = givenArray.length;

        for (int i = 1; i < length - 1; i = i + 2) {
            if ( i + 1 > length) {
                if (givenArray[i] > max) max = givenArray[i];
                if (givenArray[i] < min) min = givenArray[i];
            }
            if (givenArray[i] > givenArray[i+1]) {
                if (givenArray[i] > max) max = givenArray[i];
                if (givenArray[i + 1] < min) min = givenArray[i + 1];
            }
            if (givenArray[i] < givenArray[i+1]) {
                if (givenArray[i] < min) min = givenArray[i];
                if (givenArray[i+1] > max) max = givenArray[i + 1];
            }
        }
        System.out.println("Original array: "+ Arrays.toString(givenArray));
        System.out.println("Maximum value in the array is: " + max);
        System.out.println("Minimum value in the array is: " + min);
    }
}
