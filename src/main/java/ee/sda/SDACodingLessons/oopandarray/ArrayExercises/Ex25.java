package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.ArrayList;

public class Ex25 {
    public void solution() {
//Write a Java program to find common elements from three sorted (in
//non-decreasing order) arrays.
        ArrayList<Integer> common = new ArrayList<Integer>();
        int array1[] = {2, 5, 7, 34, 72, 74};
        int array2[] = {7, 74, 82, 85, 942};
        int array3[] = {5, 7, 38, 74, 294};
        int i = 0, j = 0, k = 0;

        while (i < array1.length && j < array2.length && k < array3.length) {
            if (array1[i] == array2[j] && array2[j] == array3[k]) {
                common.add(array1[i]);
                i++;
                j++;
                k++;
            } else if (array1[i] < array2[j]) {
                i++;
            } else if (array2[j] < array3[k]) {
                j++;
            } else
                k++;
        }
        System.out.println("The common elements in three arrays (in non-decreasing order) are: ");
        System.out.println(common);
    }
}
