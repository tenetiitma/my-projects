package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.ArrayList;
import java.util.Arrays;

public class Ex20 {
    public void solution(){
//  Write a Java program to convert an array to ArrayList.

        String[] givenArray = new String[] {"Hello", "this", "is", "a", "test."};
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(givenArray));

        System.out.println(list);
    }
}
