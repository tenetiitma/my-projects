package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

public class Ex18 {
//    Write a Java program to find the second smallest element in an array
    public static int secondSmallest(int[] b, int length){

        int temp;

        for (int i = 0; i < length; i++) {
            for (int j = i+1; j < length; j++) {
                if (b[i] > b[j]){
                    temp = b[i];
                    b[i] = b[j];
                    b[j] = temp;
                }
            }
        }
        return b[1];
    }

    public void solution() {

        int[] givenArray = {5, 32, 8, 1, 6, 54};
        System.out.println("Second smallest element is: " + secondSmallest(givenArray, 6));
    }
}
