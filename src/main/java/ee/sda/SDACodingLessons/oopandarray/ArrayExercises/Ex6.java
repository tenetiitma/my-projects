package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

public class Ex6 {
    public void solution() {
        // Write a Java program to find the index of an array element.

        char[] array = {'m', 't', '2', '5', 'o'};

        System.out.println(new String(array).indexOf('5'));
    }
}
