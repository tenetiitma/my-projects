package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Scanner;

public class Ex19 {
    public void solution() {
// Write a Java program to add two matrices of the same size.

        Scanner scanner = new Scanner(System.in);
        int a, b, c, d;

        System.out.println("Input number of rows in Matrix: ");
        a = scanner.nextInt();

        System.out.println("Input number of columns in Matrix: ");
        b = scanner.nextInt();

        int array1[][] = new int[a][b];
        int array2[][] = new int[a][b];
        int sum[][] = new int[a][b];

        System.out.println("Input elements of first Matrix: ");

        for (c = 0; c < a; c++)
            for (d = 0; d < b; d++)
                array1[c][d] = scanner.nextInt();

        System.out.println("Input elements of the second Matrix: ");

        for (c = 0; c < a; c++)
            for (d = 0; d < b; d++)
                array2[c][d] = scanner.nextInt();

        for (c = 0; c < a; c++)
            for (d = 0; d < b; d++)
                sum[c][d] = array1[c][d] + array2[c][d];

        System.out.println("The sum of the matrices is: ");

        for (c = 0; c < a; c++) {
            for (d = 0; d < b; d++)
                System.out.print(sum[c][d] + "\t");

        System.out.println();
        }
    }
}