package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

public class Ex17 {
    public static int secondLargest(int[] b, int length) {
        // Write a Java program to find the second largest element in an array.

        int temp;

        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                if (b[i] > b[j]) {
                    temp = b[i];
                    b[i] = b[j];
                    b[j] = temp;
                }
            }
        }
        return b[length - 2];
    }

    public void solution() {

        int[] givenArray = {1, 5, 2, 63, 6, 29, 86};
        System.out.println("Second largest: " + secondLargest(givenArray, 7));
    }
}

