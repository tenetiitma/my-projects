package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;

public class Ex1 {
    public void solution(){
        // Write a Java program to sort a numeric array and a string array.

        int[] array1 = {12, 34, 56433, 543, 3,533 , 5323, 1};

        String[] array2 = {"Hello", "Animal", "Tene", "Java", "Coding"};

        System.out.println("The original Array1: " + Arrays.toString(array1));
        Arrays.sort(array1);
        System.out.println("Sorted Array1: " + Arrays.toString(array1));
        System.out.println("The original Array2: " + Arrays.toString(array2));
        Arrays.sort(array2);
        System.out.println("Sorted Array2: " + Arrays.toString(array2));

    }
}
