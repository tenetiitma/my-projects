package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;

public class Ex23 {
    public void solution() {
//Write a Java program to test the equality of two arrays.

        int array1[] = {2, 5, 53, 73, 2, 77};
        int array2[] = {5, 2, 53, 73, 2, 77};

        if (Arrays.equals(array1, array2)) {
            System.out.println("These arrays are equal");
        }
        else {
            System.out.println("These arrays are not equal");
        }
    }
}
