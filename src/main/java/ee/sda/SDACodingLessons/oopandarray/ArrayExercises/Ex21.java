package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.ArrayList;

public class Ex21 {
    public void solution() {
//  Write a Java program to convert an ArrayList to an array.

        ArrayList<String> list = new ArrayList<String>();

        list.add("This");
        list.add("is");
        list.add("a");
        list.add("test.");

        String[] givenArray = new String[(list.size())];

        list.toArray(givenArray);

        for (String string : givenArray) {
            System.out.println(string);
        }
    }
}
