package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;

public class Ex24 {
    static int findTheMissingNumber(int givenArray[], int n) {
//         Write a Java program to find a missing number in an array.

        int total = (n + 1) * (n + 2) / 2;

        for (int i = 0; i < n; i++) {
            total -= givenArray[i];
        }
        return total;
    }

    public void solution() {

        int givenArray[] = {1, 2, 3, 4, 5, 7, 8};
        System.out.println("given array is: " + Arrays.toString(givenArray));
        int missingNumber = findTheMissingNumber(givenArray, 7);
        System.out.println("The missing number is: " + missingNumber);
    }
}
