package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;

public class Ex16 {
    public void solution() {
//          Write a Java program to remove duplicate elements from an array.

        int[] givenArray = {20, 58, 58, 358, 5, 20, 59};

        System.out.println("Original array is: ");

        for (int i = 0; i < givenArray.length; i++) {
            System.out.print(givenArray[i] + "\t");
        }

        int no_unique_elements= givenArray.length;

        for (int i = 0; i < no_unique_elements; i++) {
            for (int j = i+1; j < no_unique_elements; j++) {
                if (givenArray[i] == givenArray[j]) {
                    givenArray[j] = givenArray[no_unique_elements-1];
                    no_unique_elements--;
                    j--;
                }
            }
        }

        int[] array = Arrays.copyOf(givenArray, no_unique_elements);

        System.out.println();
        System.out.println("Array with no duplicates: ");

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }
    }
}