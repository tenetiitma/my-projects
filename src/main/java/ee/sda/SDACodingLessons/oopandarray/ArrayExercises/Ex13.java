package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;

public class Ex13 {
    public void solution() {
//        Write a Java program to find the duplicate values of an array of string
//        values.
        String[] givenArray = {"I'm a cat", "I'm still a cat", "Now I'm a dog", "I'm a cat"};

        System.out.println("Given array: " + Arrays.toString(givenArray));

        Arrays.sort(givenArray);
        for (int i = 0; i < givenArray.length-1; i++) {
            boolean duplicate = false;
            while (givenArray[i+1] == givenArray[i]){
                i++;
                duplicate = true;
            }
            if (duplicate) System.out.println("This is a duplicate: " + givenArray[i]);
        }
    }
}
