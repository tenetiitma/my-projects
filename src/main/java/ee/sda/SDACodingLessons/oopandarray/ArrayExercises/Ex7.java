package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;

public class Ex7 {
    public void solution() {
//        Write a Java program to remove a specific element from an array.

        char[] array = {'2', '5', 't', 'u', 'f'};
        System.out.println("Original array: " + Arrays.toString(array));

        int removeIndex = 2;

        for (int i = removeIndex; i < array.length - 1; i++) {
            array[i] = array[i+1];
        }

        System.out.println("After removing the third element: " + Arrays.toString(array));
    }
}
