package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;

public class Ex12 {
    public void solution() {

        int[] givenArray = {4, 32, 532, 53, 32, 635, 532, 6};

        System.out.println("Given array: " + Arrays.toString(givenArray));

        Arrays.sort(givenArray);
        for (int i = 0; i < givenArray.length - 1; i++) {
            boolean duplicate = false;
            while (givenArray[ i + 1] == givenArray[i]) {
                i++;
                duplicate = true;
            }
            if (duplicate) System.out.println("This is a duplicate: " + givenArray[i]);
         }
    }
}
