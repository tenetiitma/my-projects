package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;

public class Ex8 {
    public void solution() {
// Write a Java program to copy an array by iterating the array.
        int[] sourceArray = {10, 5, 24, 5, 69, 4, 800};
        int[] copiedArray = new int[7];

        System.out.println("Source array: " + Arrays.toString(sourceArray));

        for (int i = 0; i < sourceArray.length; i++) {
            copiedArray[i] = sourceArray[i];
        }
        System.out.println("Copied array: " + Arrays.toString(copiedArray));
    }
}
