package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;
import java.util.HashSet;

public class Ex14 {
    public void solution() {
//          Write a Java program to find the common elements between two arrays
//          (string values)

        String[] arrayNumOne = {"blue", "green", "apple", "milk", "soy"};
        String[] arrayNumTwo = {"blueberry", "apple", "fish", "milk", "potato", "train"};

        System.out.println("First array: " + Arrays.toString(arrayNumOne));
        System.out.println("Second array: " + Arrays.toString(arrayNumTwo));

        HashSet<String> set = new HashSet<String>();

        for (int i = 0; i < arrayNumOne.length; i++) {
            for (int j = 0; j < arrayNumTwo.length; j++) {
                if (arrayNumOne[i].equals(arrayNumTwo[j])) {
                    set.add(arrayNumOne[i]);
                }
            }
        }
        System.out.println("Common element: " + set);
    }
}
