package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

public class Ex2 {
    public void solution(){
//        Write a Java program to sum values of an array

        int[] array = {1, 6, 34, 6, 9, 169};

        int sum = 0;

        for (int num : array) {
            sum = sum+num;
        }

        System.out.println("The sum of the values is: " + sum);

    }
}
