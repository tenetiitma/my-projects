package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;

public class Ex9 {
    public void solution() {
//        Write a Java program to insert an element (specific position) into an array.

        int[] givenArray = {1, 2, 3, 4, 6, 7, 8, 9};

        int indexNumber = 4;
        int insertedElement = 5;

        System.out.println("Given array: " + Arrays.toString(givenArray));

        for (int i = givenArray.length - 1; i > indexNumber; i--) {
            givenArray[i] = givenArray[i-1];
        }
        givenArray[indexNumber] = insertedElement;
        System.out.println("New array with the added numbeR: " + Arrays.toString(givenArray));
    }
}
