package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;

public class Ex11 {
    public void solution() {

        int[] givenArray = {295, 2982, 582, 572, 57249, 852, 72, 582, 8, 275};

        System.out.println("Original array is: " + Arrays.toString(givenArray));

        for (int i = 0; i < givenArray.length / 2; i++) {
            int temp = givenArray[i];

            givenArray[i] = givenArray[givenArray.length - i - 1];
            givenArray[givenArray.length - i - 1] = temp;
        }
        System.out.println("Reversed array is: " + Arrays.toString(givenArray));
    }
}
