package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

public class Ex3 {
    public void solution(){

        int[][] grid = new int[10][10];

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                System.out.printf("-", grid[i][j]);
            }
            System.out.println();
        }
    }
}
