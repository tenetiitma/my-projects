package ee.sda.SDACodingLessons.oopandarray.ArrayExercises;

import java.util.Arrays;
import java.util.Scanner;

public class Ex22 {
    public void solution() {
//Write a Java program to find all pairs of elements in an array whose sum
//is equal to a specified number.
        Scanner scanner = new Scanner(System.in);

        int givenArray[] = {2, 5, 12, 63, 32, 392, 58, 6, 48};
        System.out.println("Given array is: " + Arrays.toString(givenArray));

        System.out.println("Please input the sum of two numbers in the given array:");
        int inputNumber = scanner.nextInt();

        for (int i = 0; i < givenArray.length; i++) {
            for (int j = i+1; j < givenArray.length ; j++) {
                if (givenArray[i] + givenArray[j] == inputNumber) {
                    System.out.println(givenArray[i] + " + " + givenArray[j] + " = " + inputNumber);
                }
            }
        }
    }
}
