package ee.sda.SDACodingLessons.oopandarray;

import ee.sda.SDACodingLessons.OOP.Employee;

public class EmployeeTraverse {

    //Show all employee's first names

    public void showAlEmployeesFirstName(Employee[] employees){

        System.out.println("We have these employees: ");
        System.out.println();

        for (int i = 0; i < employees.length; i++) {
            if(employees[i] != null) {
            System.out.print(employees[i].getFirstName() + " age: " + employees[i].getAge() + ", ");
        }
    }
    }
}
