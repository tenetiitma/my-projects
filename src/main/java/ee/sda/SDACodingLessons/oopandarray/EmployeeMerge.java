package ee.sda.SDACodingLessons.oopandarray;

import ee.sda.SDACodingLessons.OOP.Employee;

public class EmployeeMerge {

    public Employee[] mergeTwoEmployeesList(Employee[] employees1,
                                            Employee[] employees2) {

        Employee[] mergedArrayOfEmployees = new Employee[employees1.length + employees2.length];

        int index = 0;
        // 3, 4, 5, 5
        for (int i = 0; i < employees1.length; i++) {
            if (employees1[i] != null) {
                mergedArrayOfEmployees[index] = employees1[i];
                index++;
            }
        }

        // 3, 4, 5, 5, 6, 7, 8
        for (int i = 0; i < employees2.length; i++) {
            if (employees2[i] != null) {
                mergedArrayOfEmployees[index] = employees2[i];
                index++;
            }
        }
        return mergedArrayOfEmployees;
    }
}